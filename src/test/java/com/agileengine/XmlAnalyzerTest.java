package com.agileengine;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class XmlAnalyzerTest {
    private static final String ORIGINAL_SOURCE = "src/main/resources/samples/sample-0-origin.html";
    private static final String ELEMENT_ID = "make-everything-ok-button";

    @Test
    public void selfTest() {

        HtmlSimilarity htmlSimilarity = new XmlAnalyzer(ORIGINAL_SOURCE, ORIGINAL_SOURCE);
        Optional<String> similarElement = htmlSimilarity.findSimilarElement(ELEMENT_ID);

        String expected = "/html/body/div/div/div[3]/div[1]/div/div[2]/a";
        assertEquals(expected, similarElement.get());
    }

    @Test
    public void sample1Test() {
        String actualSource = "src/main/resources/samples/sample-1-evil-gemini.html";

        HtmlSimilarity htmlSimilarity = new XmlAnalyzer(ORIGINAL_SOURCE, actualSource);
        Optional<String> similarElement = htmlSimilarity.findSimilarElement(ELEMENT_ID);

        String expected = "/html/body/div/div/div[3]/div[1]/div/div[2]/a[2]";
        assertEquals(expected, similarElement.get());
    }

    @Test
    public void sample2Test() {
        String actualSource = "src/main/resources/samples/sample-2-container-and-clone.html";

        HtmlSimilarity htmlSimilarity = new XmlAnalyzer(ORIGINAL_SOURCE, actualSource);
        Optional<String> similarElement = htmlSimilarity.findSimilarElement(ELEMENT_ID);

        String expected = "/html/body/div/div/div[3]/div[1]/div/div[2]/div/a";
        assertEquals(expected, similarElement.get());
    }

    @Test
    public void sample3Test() {
        String actualSource = "src/main/resources/samples/sample-3-the-escape.html";

        HtmlSimilarity htmlSimilarity = new XmlAnalyzer(ORIGINAL_SOURCE, actualSource);
        Optional<String> similarElement = htmlSimilarity.findSimilarElement(ELEMENT_ID);

        String expected = "/html/body/div/div/div[3]/div[1]/div/div[3]/a";
        assertEquals(expected, similarElement.get());
    }


    @Test
    public void sample4Test() {
        String actualSource = "src/main/resources/samples/sample-4-the-mash.html";

        HtmlSimilarity htmlSimilarity = new XmlAnalyzer(ORIGINAL_SOURCE, actualSource);
        Optional<String> similarElement = htmlSimilarity.findSimilarElement(ELEMENT_ID);

        String expected = "/html/body/div/div/div[3]/div[1]/div/div[3]/a";
        assertEquals(expected, similarElement.get());
    }

    @Test
    public void elementNotFoundTest() {
        HtmlSimilarity htmlSimilarity = new XmlAnalyzer(ORIGINAL_SOURCE, ORIGINAL_SOURCE);
        Optional<String> similarElement = htmlSimilarity.findSimilarElement("wrong-element-id");

        assertEquals(Optional.empty(), similarElement);
    }

    @Test(expected = XmlAnalyzerException.class)
    public void fileNotFoundTest() {
        HtmlSimilarity htmlSimilarity = new XmlAnalyzer(ORIGINAL_SOURCE, "wrong-path-to-actual-file");
        htmlSimilarity.findSimilarElement(ELEMENT_ID);
    }
}
