package com.agileengine;

import java.util.Optional;

public interface HtmlSimilarity {

    Optional<String> findSimilarElement(String elementId);

}
