package com.agileengine;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class XmlAnalyzer implements HtmlSimilarity {
    private static int MIN_ATTRIBUTES_MATCH = 3;
    private static String CHARSET_NAME = "utf8";

    private static Logger LOG = LoggerFactory.getLogger(XmlAnalyzer.class);

    private final String originalFilePath;
    private final String actualFilePath;


    public XmlAnalyzer(String originalFilePath, String actualFilePath) {
        validateFilePath(originalFilePath, actualFilePath);

        this.originalFilePath = originalFilePath;
        this.actualFilePath = actualFilePath;
    }


    @Override
    public Optional<String> findSimilarElement(String elementId) {
        Optional<Element> originalElementOpt = findElementById(new File(originalFilePath), elementId);

        if (!originalElementOpt.isPresent()){
            LOG.warn("Element {} not found in original file", elementId);
            return Optional.empty();
        }

        Element originalElement = originalElementOpt.get();
        LOG.info("Original Element:\n{}\n", originalElement);

        Elements actualElements = findElementsByTag(new File(actualFilePath), originalElement.tagName());

        Map<Integer, Element> matchMap = actualElements.stream()
                .collect(Collectors.toMap(
                        element -> attributesMatchCount(originalElement, element),
                        element -> element,
                        (el1, el2) -> el1));

        Integer bestMatch = Collections.max(matchMap.keySet());

        if (bestMatch < MIN_ATTRIBUTES_MATCH) {
            return Optional.empty();
        }

        Element actualElement = matchMap.get(bestMatch);
        LOG.info("Actual Element (similarity level: {}):\n{}\n", bestMatch, actualElement);

        return Optional.of(buildXPath(actualElement));
    }

    private int attributesMatchCount(Element elementA, Element elementB) {
        int counter = 0;

        if (elementA.text() != null && elementA.text().equals(elementB.text())) {
            counter++;
        }

        for (Attribute attrA : elementA.attributes()) {
            String valB = elementB.attributes().get(attrA.getKey());
            if (attrA.getValue() != null && attrA.getValue().equals(valB)) {
                counter++;
            }
        }

        return counter;
    }

    private Elements findElementsByTag(File htmlFile, String tag) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            return doc.getElementsByTag(tag);
        } catch (IOException e) {
            String message = String.format("Unable to read %s file: %s ", htmlFile.getAbsolutePath(), e.getMessage());
            throw new XmlAnalyzerException(message);
        }
    }


    private Optional<Element> findElementById(File htmlFile, String targetElementId) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());
            Element element = doc.getElementById(targetElementId);

            if (element == null) {
                return Optional.empty();
            }

            return Optional.of(element);
        } catch (IOException e) {
            String message = String.format("Unable to read %s file: %s ", htmlFile.getAbsolutePath(), e.getMessage());
            throw new XmlAnalyzerException(message);
        }
    }

    private String buildXPath(Element element) {
        StringBuilder xpath = new StringBuilder();
        Element el = element;

        while (!el.tagName().equals("#root")) {
            xpath.insert(0, getTag(el));
            el = el.parent();
        }

        return xpath.toString();
    }

    private String getTag(Element element) {
        int totalTags = 0;
        int elementNumber = 0;

        for (Element child : element.parent().children()) {
            if (child.tagName().equals(element.tagName())) totalTags++;

            if (child == element) elementNumber = totalTags;
        }
        if (elementNumber == 1 && totalTags == 1) {
            return "/" + element.tagName();
        } else {
            return "/" + element.tagName() + "[" + elementNumber + "]";
        }
    }

    private void validateFilePath(String originalFilePath, String actualFilePath) {
        if (originalFilePath == null || originalFilePath.isEmpty()) {
            throw new XmlAnalyzerException("Original File Path could not be empty");
        }

        if (actualFilePath == null || actualFilePath.isEmpty()) {
            throw new XmlAnalyzerException("Actual File Path could not be empty");
        }
    }

}