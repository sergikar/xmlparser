package com.agileengine;

public class XmlAnalyzerException  extends RuntimeException{

    public XmlAnalyzerException(String message) {
        super(message);
    }
}
