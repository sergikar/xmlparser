package com.agileengine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class XmlAnalyzerDemo {
    private static Logger LOG = LoggerFactory.getLogger(XmlAnalyzerDemo.class);

    private static final String DEFAULT_ELEMENT_ID = "make-everything-ok-button";

    public static void main(String[] args) {
        try {
            if (args.length < 2) {
                throw new IllegalArgumentException("Should be at least 2 arguments");
            }

            String targetElementId = DEFAULT_ELEMENT_ID;
            if (args.length > 2) {
                targetElementId = args[2];
            }

            HtmlSimilarity htmlSimilarity = new XmlAnalyzer(args[0], args[1]);
            Optional<String> similarElement = htmlSimilarity.findSimilarElement(targetElementId);

            if (similarElement.isPresent()) {
                LOG.info("XPath to the actual element:");
                System.out.println(similarElement.get());
            } else {
                LOG.warn("No similar element found for {}", targetElementId);
            }

        } catch (RuntimeException e) {
            LOG.error(e.getMessage());
        }
    }


}