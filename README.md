XML Parser Backend Project
=======================

###Technologies
- Jsoup

Build the project
------------
To build the project the next tools have to be installed:

- Git  
- Maven  
- JDK 8

To build run the next commands:  
```
git clone git@bitbucket.org:sergikar/xmlparser.git
cd xmlparser
mvn package
```

####Output Artifacts:
``` 
./xmlparser/target/xml-parser.jar  
```  

####How To Run
`java -jar <full_path_to xml-parser.jar>  <input_origin_file_path> <input_other_sample_file_path>`

`java -jar <full_path_to xml-parser.jar>  <input_origin_file_path> <input_other_sample_file_path> <element_id>`

####Example
`java -jar ./target/xml-parser.jar ./src/main/resources/samples/sample-0-origin.html ./src/main/resources/samples/sample-4-the-mash.html`